<?php

namespace App\Imports;

use App\MobileUser;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new MobileUser([
            'name'     => $row['name'],
            'email'    => $row['email']
        ]);
    }
}
