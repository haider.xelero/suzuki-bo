<?php

namespace App\Exports;

use App\MobileUser;
use Maatwebsite\Excel\Concerns\FromCollection;
use Carbon\Carbon;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $user = MobileUser::all('id','nom','prenom','telephone','region','adresse','email','created_at');
        foreach($user as $u)
        {
            $u->created_at = Carbon::parse($u->created_at)->format("Y-m-d");
        }
        return $user;
    }
}
