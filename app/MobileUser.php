<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MobileUser extends Model
{
    protected $hidden = [
        'mot_de_passe','updated_at'
    ];
}
