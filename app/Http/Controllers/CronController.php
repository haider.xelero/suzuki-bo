<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use App\MobileUser;
use App\Notification;
use App\Vehicule;
use \TCG\Voyager\Models\Role;

class CronController extends Controller
{

    private $UrlPushNotif = "https://exp.host/--/api/v2/push/send";

    public function birthday()
    {
        try
        {
            $today=now();   
            $user=MobileUser::whereMonth('date_de_naissance',$today->month)
                    ->whereDay('date_de_naissance',$today->day)
                    ->get();
            if(count($user)>0)
            {
                foreach($user as $u)
                {
                    $this->insertNotification("Happy birthday", null, "2TXT", $today, "blue", $u->id);
                    $this->sendNotificationToUser($u->id, "Happy birthday", "Suzuki Tunisie vous souhaite une bonne année");
                }
                return $user;
            }
            else
                return [];
        }
        catch(Exception $e)
        {
            return [];
        }
    }

    public function Rappel_km()
    {
        try
        {
            //return Carbon::now()->format('D') ; //Wed Sun
            $today=now();
            if(Carbon::now()->format('D') == "Sun") //you should put Sun
            {
                $user = MobileUser::all();
                foreach($user as $u)
                {
                    $this->insertNotification("Pensez à mettre à jour le kilométrage de votre véhicule", null, "2TXT", $today, "red", $u->id);
                    $this->sendNotificationToUser($u->id, "Pensez à mettre à jour le kilométrage de votre véhicule", "Merci de mettre à jour le kilométrage");
                }
            }
        }
        catch(Exception $e)
        {
            return response()->json(["Error" => "Error exception"]);
        }
    }

    public function Rappel_date_assurance()
    {
        try
        {
            $vehicules = Vehicule::all();
            foreach($vehicules as $v)
            {
                if($this->DateExpire($v->date_assurance, Carbon::now()))
                {
                    $this->insertNotification("Renouvellement assurance", null, "2TXT", $v->date_assurance, "blue", $v->id_user);
                    $this->sendNotificationToUser($v->id_user, "Renouvellement d'assurance", "Merci de re-nouveller l'assurance le".$v->date_assurance);
                }
            }
        }
        catch(Exception $e)
        {
            return response()->json(["error" => "Exception error"]);
        }
    }

    public function Rappel_date_vingette()
    {
        try
        {
            $vehicules =  Vehicule::all();
            foreach($vehicules as $v)
            {
                if(isset($v->date_vignette))
                {
                    //echo ApisController::getDateVignette($v->date_vignette,$v->id).' ';
                    if($this->DateExpire(ApisController::getDateVignette($v->date_vignette,$v->id), Carbon::now()))
                    {
                        $this->insertNotification("Renouvellement des vignettes", null, "2TXT", ApisController::getDateVignette($v->date_vignette,$v->id), "blue", $v->id_user);
                        $this->sendNotificationToUser($v->id_user, "Renouvellement des vignettes", "Merci de re-nouveller des vignettes,  le".ApisController::getDateVignette($v->date_vignette,$v->id));
                    }
                }
            }
            return;
        }
        catch(Exception $e)
        {
            return [];
        }
    }

    public function Rappel_date_visite_technique()
    {
        try
        {
            $vehicules =  Vehicule::all();
            foreach($vehicules as $v)
            {
                if(isset($v->date_circulation))
                {
                    if($this->DateExpire(ApisController::getVisiteTechnique($v->date_circulation), Carbon::now()))
                    {
                        $this->insertNotification("Visite technique", null, "2TXT", ApisController::getVisiteTechnique($v->date_circulation), "blue", $v->id_user);
                        $this->sendNotificationToUser($v->id_user, "Visite technique", "Merci de faire la visite technique,  le".ApisController::getVisiteTechnique($v->date_circulation));
                    }
                }
            }
            return;

        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    public function deleteOldNotification($id)
    {
        /*
        try
        {  updated  
            if(!is_numeric($id))
            {
                return response()->json
                (
                    [
                        "code" => 0,
                        "status" => "error",
                        "message" => "You should enter a numuric value"
                    ]
                );
            }
            else
            {
                $beforeNDays = Carbon::now()->subDays($id);
                $n = Notification::where('created_at','<',$beforeNDays)->delete();
                //$n->delete();
                return response()->json
                (
                    [
                        "code" => 1,
                        "status" => "success",
                        "message" =>"Notifications has been deleted"
                    ]
                );
            }
        }
        catch(Exception $e)
        {
            return response()->json
            (
                [
                    "code" => 0,
                    "status" => "exception",
                    "message" => $e
                ]
            );
        }
        */
    }

    public function AutoSendNotification ()
    {
        try
        {

        }
        catch(Exception $e)
        {

        }
    }

    private function insertNotification($titre, $valeur, $type, $date, $color, $id_user)
    {
        $notif = new Notification();
        $notif->titre = $titre;
        $notif->valeur = $valeur;
        $notif->type = $type;
        $notif->date = $date;
        $notif->color = $color;
        $notif->id_user = $id_user;
        $notif->save();
    }

    private function DateExpire($date,$now) 
    {
        $nowDay = Carbon::now()->format('Y-m-d');
        //return $now->diffInDays($date);
        return (($date>=$nowDay) && ( ($now->diffInDays($date) == 0) || ($now->diffInDays($date) == 1) || ($now->diffInDays($date)== 3) ||($now->diffInDays($date)== 7) || ($now->diffInDays($date)== 15) ||($now->diffInDays($date)== 30) ) ) ;
    }

    public function PushNotif(string $token, string $title, string $body)
    {
        $token1 = json_decode($token);
        $response = Http::withHeaders([
            'host' => 'exp.host',
            'accept' => 'application/json',
            'accept-encoding' => 'gzip, deflate',
            'content-type' => 'application/json',
        ])->post($this->UrlPushNotif, [
            'to' => $token1,
            'title' => $title,
            'body' => $body
        ]);
        return $response;
    }

    private function sendNotificationToUser($id, $title, $content)
    {
        $m = MobileUser::find($id);
        if(isset($m->token_device))
        {
            $array = json_decode($m->token_device);
            foreach($array as $v)
            {
                if(($v != "ExponentPushToken[SIMULATOR]") or (!isset($v)))
                {
                    $token = "$v";
                    $token=str_replace('"','\"',$token);
                    $token = '"'.$token.'"';
                    $token = '['.$token.']';
                    $this->PushNotif($token, $title, $content);
                }
            }
        }
    }

    public function getRole()
    {
        return Role::all();
    }
}
