<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\ManuelNotification ;
use App\ManuelleUser;
use App\Vehicule;
use App\MobileUser;
use App\Notification;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use ReflectionClass;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Database\Schema\Table;
use TCG\Voyager\Database\Types\Type;
use TCG\Voyager\Events\BreadAdded;
use TCG\Voyager\Events\BreadDeleted;
use TCG\Voyager\Events\BreadUpdated;
use TCG\Voyager\Facades\Voyager;

class WebController extends Controller
{
    private $UrlPushNotif = "https://exp.host/--/api/v2/push/send";
        
    // public function sendManuelleNotif(Request $request)
    // {
    //     try
    //     {
    //         $m = new ManuelNotification();
    //         $m->titre = $request->titre;
    //         $m->valeur = $request->valeur;
    //         $m->age = $request->age;
    //         $m->region = $request->region;
    //         $m->save();
    //         if(isset($request->manuel_notification_belongstomany_vehicule_relationship))
    //         {
    //             foreach($request->manuel_notification_belongstomany_vehicule_relationship as $v)
    //             {
    //                 $mu = new ManuelleUser();
    //                 $mu->manuel_notification_id = $m->id;
    //                 $mu->vehicule_id = $v; //this the id of vehicule
    //                 $mu->save();
    //             }
    //         }
    //         if((!isset($request->age)) AND (!isset($request->region)) AND ((!isset($request->manuel_notification_belongstomany_vehicule_relationship))))
    //         {
    //             $user = MobileUser::where('new','false')->get();
    //             foreach($user as $u)
    //             {
    //                 $this->insertNotification("Notification", $request->valeur , "3TXT", Carbon::now()->format('Y-m-d'), "blue", $u->id);
    //                 $this->sendNotificationToUser($u->id, $request->titre, $request->valeur);
    //             }
    //         }
    //         elseif((!isset($request->age)) AND (isset($request->region)) AND ((!isset($request->manuel_notification_belongstomany_vehicule_relationship))))
    //         {
    //             $user = MobileUser::where('region',$request->region)->where('new','false')->get();
    //             foreach($user as $u)
    //             {
    //                 $this->insertNotification("Notification", $request->valeur , "3TXT", Carbon::now()->format('Y-m-d'), "blue", $u->id);
    //                 $this->sendNotificationToUser($u->id, $request->titre, $request->valeur);
    //             }
    //         }
    //         elseif((isset($request->age)) AND (!isset($request->region)) AND ((!isset($request->manuel_notification_belongstomany_vehicule_relationship))))
    //         {
    //             $year_diff1 = Carbon::now()->year - $request->age;
    //             $year_diff2 = Carbon::now()->year - ($request->age + 1);
    //             //return $year_diff2;
    //             $listVehicule = Vehicule::whereBetween('date_circulation', ["$year_diff1-01-01", "$year_diff1-12-31"])->OrWhereBetween('date_circulation', ["$year_diff2-01-01", "$year_diff2-12-31"])->get();
    //             if(!isset($listVehicule))
    //                 return redirect()->back()->with('error', 'Pas de notification pour les clients');
    //             else
    //             {
    //                 $tab = [];
    //                 foreach($listVehicule as $v)
    //                 {
    //                     $user = MobileUser::where('id',$v->id_user)->where('new','false')->get();
    //                     if(isset($user))
    //                     {
    //                         $this->insertNotification("Notification", $request->valeur , "3TXT", Carbon::now()->format('Y-m-d'), "blue", $user->id);
    //                         $this->sendNotificationToUser($user->id, $request->titre, $request->valeur);
    //                     }
    //                 }
    //             }
    //         }
    //         elseif((isset($request->age)) AND (isset($request->region)) AND ((!isset($request->manuel_notification_belongstomany_vehicule_relationship))))
    //         {
    //             $year_diff1 = Carbon::now()->year - $request->age;
    //             $year_diff2 = Carbon::now()->year - ($request->age + 1);
    //             //return $year_diff2;
    //             $listVehicule = Vehicule::whereBetween('date_circulation', ["$year_diff1-01-01", "$year_diff1-12-31"])->OrWhereBetween('date_circulation', ["$year_diff2-01-01", "$year_diff2-12-31"])->get();
    //             if(!isset($listVehicule))
    //                 return redirect()->back()->with('error', 'Pas de notification pour les clients');
    //             else
    //             {
    //                 //return $listVehicule;
    //                 foreach($listVehicule as $v)
    //                 {
    //                     $user = MobileUser::where('id',$v->id_user)
    //                             ->where('region',$request->region)
    //                             ->where('new','false')
    //                             ->first();
    //                     if(isset($user))
    //                     {
    //                         $this->insertNotification("Notification", $request->valeur , "3TXT", Carbon::now()->format('Y-m-d'), "blue", $user->id);
    //                         $this->sendNotificationToUser($user->id, $request->titre, $request->valeur);
    //                     }
    //                 }
    //             }
    //         }
    //         elseif(isset($request->manuel_notification_belongstomany_vehicule_relationship))
    //         {
    //             $vehicules = Vehicule::find($request->manuel_notification_belongstomany_vehicule_relationship);
    //             foreach($vehicules as $v)
    //             {
    //                 $user = MobileUser::where('id',$v->id_user)->where('new','false')->first(); //first instead get
    //                 if(isset($user))
    //                 {
    //                     $this->insertNotification("Notification", $request->valeur , "3TXT", Carbon::now()->format('Y-m-d'), "blue", $user->id);
    //                     $this->sendNotificationToUser($user->id, $request->titre, $request->valeur);
    //                 }
    //             }
    //         }
    //         return redirect()->back()->with('message', 'La notification a été envoyé avec success');
    //     }
    //     catch(Exception $e)
    //     {
    //         return redirect()->back()->with('error', 'There is no user who have what you want to notification');
    //     }
    // }

    public function sendManuelleNotif(Request $request)
    {
        //ini_set('max_execution_time', 300); // 5 minutes
        if((!isset($request->manuel_notification_belongstomany_vehicule_relationship)) && (!isset($request->region)) && (!isset($request->telephone)) && (!isset($request->email)) )
        {
            foreach (DB::table('Mobile_users')->cursor() as $user)
            {
                $this->insertNotification("Notification", $request->valeur , "3TXT", Carbon::now()->format('Y-m-d'), "blue", $user->id);
                $this->sendNotificationToUser($user->id, $request->titre, $request->valeur);
            }
        }
        if(!isset($request->region))
            $request->region = 0;
        if(!isset($request->telephone))
            $request->telephone = "";
        if(!isset($request->email))
            $request->email = "";
        $m = new ManuelNotification();
        $m->titre = $request->titre;
        $m->valeur = $request->valeur;
        $m->age = $request->age;
        $m->region = $request->region;
        $m->save();
        if(isset($request->manuel_notification_belongstomany_vehicule_relationship))
        {
            foreach($request->manuel_notification_belongstomany_vehicule_relationship as $v)
            {
                $mu = new ManuelleUser();
                $mu->manuel_notification_id = $m->id;
                $mu->vehicule_id = $v; //this the id of vehicule
                $mu->save();
            }
        }
        $id_users = [];
        if(isset($request->manuel_notification_belongstomany_vehicule_relationship))
        {
            $vehicules = Vehicule::find($request->manuel_notification_belongstomany_vehicule_relationship);
        
            foreach($vehicules as $v)
                $id_users[] = $v->id_user;
        }
        $users = MobileUser::whereIn('id',$id_users)->orWhere('email', $request->email)->orWhere('telephone',$request->telephone)->orWhere('region',$request->region)->get();
        foreach($users as $user)
        {
            $this->insertNotification("Notification", $request->valeur , "2TXT", Carbon::now()->format('Y-m-d'), "blue", $user->id);
            $this->sendNotificationToUser($user->id, $request->titre, $request->valeur);
        }
        return redirect()->back()->with('message', 'La notification a été envoyé avec success');
    }

    private function insertNotification($titre, $valeur, $type, $date, $color, $id_user)
    {
        $notif = new Notification();
        $notif->titre = $titre;
        $notif->valeur = $valeur;
        $notif->type = $type;
        $notif->date = $date;
        $notif->color = $color;
        $notif->id_user = $id_user;
        $notif->save();
    }

    public function PushNotif(string $token, string $title, string $body)
    {
        $token1 = json_decode($token);
        $response = Http::withHeaders([
            'host' => 'exp.host',
            'accept' => 'application/json',
            'accept-encoding' => 'gzip, deflate',
            'content-type' => 'application/json',
        ])->post($this->UrlPushNotif, [
            'to' => $token1,
            'title' => $title,
            'body' => $body
        ]);
        return $response;
    }

    private function sendNotificationToUser($id, $title, $content)
    {
        $m = MobileUser::find($id);
        if(isset($m->token_device))
        {
            $array = json_decode($m->token_device);
            foreach($array as $v)
            {
                if(($v != "ExponentPushToken[SIMULATOR]") or (!isset($v)))
                {
                    $token = "$v";
                    $token=str_replace('"','\"',$token);
                    $token = '"'.$token.'"';
                    $token = '['.$token.']';
                $this->PushNotif($token, $title, $content);
                }
            }
        }
    }

    public function VO()
    {
        return View('vendor.voyager.vo.browse');
    }

    public function Count()
    {
        // $users = MobileUser::all();
        // $i = 0;
        // foreach($users as $user)
        // {
        //     echo $user->id.' ';
        // }
        // $posts = MobileUser::chunk(100, function($posts, $i){
        //     foreach ($posts as $post){
        //     }
        // });
        // foreach (MobileUser::cursor() as $post){
        //     // Process a single post
        //     echo $post->id.' ';
        //  }
        foreach (DB::table('Mobile_users')->cursor() as $post){
            // Process a single post
            echo $post->id.' ';
         } 
    }

    public function getCountUserWillNotfied(Request $request)
    {
        if((!isset($request->manuel_notification_belongstomany_vehicule_relationship)) && (!isset($request->region)) && (!isset($request->telephone)) && (!isset($request->email)) )
        {
            return MobileUser::all()->count();
        }
        if(!isset($request->region))
            $request->region = 0;
        if(!isset($request->telephone))
            $request->telephone = "";
        if(!isset($request->email))
            $request->email = "";
       
        $id_users = [];
        if(isset($request->manuel_notification_belongstomany_vehicule_relationship))
        {
            $vehicules = Vehicule::find($request->manuel_notification_belongstomany_vehicule_relationship);
        
            foreach($vehicules as $v)
                $id_users[] = $v->id_user;
        }
        $users = MobileUser::whereIn('id',$id_users)->orWhere('email', $request->email)->orWhere('telephone',$request->telephone)->orWhere('region',$request->region)->get();
        return $users->count();
    }

}
