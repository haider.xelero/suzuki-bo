<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Exception;
use App\MobileUser ;
use App\Service ;
use App\Vehicule ;
use App\Puissance ;
use App\Assurance ;
use App\Entretien ;
use App\Notification ;
use App\Publicite ;
use App\ManuelNotification ;
use App\Rappel;
use App\Region;
use App\NotifDetail;
use App\Contact;
use App\DescriptionJuridique;
use App\Modele;
use SebastianBergmann\ObjectReflector\ObjectReflector;
use \Storage;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

class ApisController extends BaseController
{
    //private $UrlGetVehicule = "http://193.95.112.188:8685/ServiceCarPro.svc/getvehicleno/";
    //private $UrlGetVehicule = "https://193.95.112.185:442/ServiceCarPro.svc/getvehicleno/";
    //private $UrlGetVehicule = "http://193.95.112.185:442/ServiceCarPro.svc/getcustomer/";
    private $UrlGetVehicule = "http://193.95.112.185:442/ServiceCarPro.svc/getvehicleno/";
    private $UrlPushNotif = "https://exp.host/--/api/v2/push/send";

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private function isValidEmail($email) //this function can verify the email format
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return false;
        return true;
    }

    //************ Function Inscription ************
    /*public function Inscription (request $request)
    {

        $token_device = json_encode([$request->token_device]) ;

        $date = date('Y-m-d H:i:s');
        $pass = Hash::make($request->mot_de_passe);

        if ($request->type_auth_api == "email")
        {
            $verifEmail = MobileUser::where('email',$request->email)->first();
        }
        else
        {
            $verifEmail = MobileUser::where('token_api',$request->token_api)->where('type_auth_api',$request->type_auth_api)->first();
        }
        // return response()->json($verifEmail);
            if (!isset($verifEmail)){
                $User = new MobileUser() ;
                if ($request->type_auth_api == "email")
                {
                    $User->email = $request->email;
                    $User->mot_de_passe = $pass;
                }
                else
                {
                    $User->token_api =  $request->token_api ;
                }

                $User->token_device = $token_device;
                $User->type_auth_api = $request->type_auth_api;
                $User->created_at = $date;
                $User->new = true ;

                $User->save() ;

                // Retourner l'objet de nouvelle client
                return response()->json($User);
            }
            else
            {
                $verifEmail->new = false ;
                return response()->json($verifEmail);
            }
        return response()->json(false);
    }*/

    public function Inscription (request $request)
    {
        if(isset($request->type_auth_api))
        {
            if($request->type_auth_api == "email")
            {
                $isUserExist = MobileUser::where('email', strtolower($request->email))->first();
                if(!isset($isUserExist))
                {
                    $User = new MobileUser() ;
                    $token_device = json_encode([$request->token_device]);
                    $User->email = strtolower($request->email);
                    $User->mot_de_passe = Hash::make($request->mot_de_passe);
                    $User->token_device = $token_device;
                    $User->type_auth_api = $request->type_auth_api;
                    $User->created_at = date('Y-m-d H:i:s');
                    $User->new = "true" ;
                    //'rappel_assurance','rappel_vignette','rappel_visite_technique'
                    $User->rappel_assurance = 2;
                    $User->rappel_vignette = 2;
                    $User->rappel_visite_technique = 2;
                    $User->save() ;
                    return response()->json($User);
                }
                //return "this user is already exist";
                return response()->json(false); //this user is already exist
            }
            //return "type_auth_api should be 'email'";
            return response()->json(false); //this user is already exist
        }
        //return "You should enter the object type_auth_api";
        return response()->json(false); //You should enter the object type_auth_api
    }

    //************ Function Auth ************
    public function Auth(Request $request)
    {
        // try
        // {
            if(isset($request->type_auth_api))
            {
                // if(isset($request->token_device))
                // {
                    switch (strtolower($request->type_auth_api))
                    {
                        case "email" :  if((isset($request->email)) AND (isset($request->mot_de_passe)))
                                        {
                                            if(!$this->isValidEmail($request->email))
                                            {
                                                return response()->json("is not email format");
                                            }
                                            else
                                            {
                                                return $this->AuthNormal($request->email,$request->mot_de_passe, $request->token_device);
                                            }
                                        }
                                        else
                                        {
                                            return response()->json
                                            ("to authenticate with the normal method you should enter your email and password");
                                        }
                        break;
                        case "gmail" :      if(isset($request))
                                            {
                                                //google_data
                                                //return $this->AuthGmail($request->data, $request->token_device);
                                                return $this->AuthGmail($request);
                                            }
                                            else
                                            {
                                                return response()->json
                                                ("The presence of the object is mandatory (it's json object)");
                                            }
                        break;
                        case "facebook" :   if(isset($request->data))
                                                return $this->AuthFacebook($request->data, $request->token_device);
                                            else
                                                return response()->json
                                                ("The presence of the object is mandatory (it's json object)");
                        break;
                        case "appel" : return $this->AuthAppel($request->data,$request->token_device);
                            break;
                        default :   return response()->json("type_auth_api is not available");
                        break;
                    }
                }
                // else
                // {
                //     return response()->json
                //     ("The presence of the device code is mandatory");
                // }
            //}
            else
            {
                return response()->json("You should enter the paramter type_auth_api");
            }
        // }
        // catch(Exception $e)
        // {
        //     return response()->json
        //     ("Exception error");
        // }
    }

    private function AuthNormal($email, $password, $token_device)
    {
        $user_mobile = MobileUser::where('email', $email)->get()->first();
        if (isset($user_mobile))
        {
            if (strtolower($user_mobile->type_auth_api) == "email")
            {
                $user_mobile = MobileUser::where('email', $email)->first();
                if (isset($user_mobile))
                {
                    if (Hash::check($password, $user_mobile->mot_de_passe))
                    {
                        if (strlen($user_mobile->token_device) > 2)
                        {
                            if (!$this->SearchTokenDevice($user_mobile->token_device, $token_device))
                            {
                                $tab = (array)json_decode($user_mobile->token_device); //we should cast the object stdClass to array
                                $tab[] = $token_device; //add new element in the table $tab
                                $user_mobile->token_device = json_encode($tab);
                            }
                        }
                        else
                        {
                            $user_mobile->token_device = json_encode([$token_device]);
                        }
                        $user_mobile->updated_at = date("Y-m-d H:m:s");
                        //$user_mobile->new = "true";
                        $user_mobile->save();
                    }
                    else
                    {
                        return response()->json
                        (
                            [
                                "message" => "Your password is not correct",
                                "response" => "Error"
                            ]
                        );
                    }
                }
                else
                {
                    return response()->json
                    (
                        [
                            'response' => 'Your email does not exist'
                        ]
                    );
                }
                return $this->getUser($user_mobile->id);
                //return response()->json($user_mobile); //user mobile connected
            }
            else
            {
                return response()->json
                (
                    [
                        "message" => "This user is already exist but he registered with other social media and or method and he using this email, please use other email address ",
                        "response" => "Error"
                    ]
                );
            }
        }
        else
        {
            return response()->json
            (
                [
                    "message" => "Your email does not exist",
                    "response" => "error"
                ]
            );
        }
    }

    //old version
    // private function AuthGmail($object, $token_device)
    // {
    //     try
    //     {
    //         $json = (stripslashes($object));
    //         $json = str_replace('“','"',$json);
    //         $json = rtrim($json, "\0");
    //         $tab = json_decode($json);
    //         if ($tab == null)
    //         {
    //             return response()->json
    //             (
    //                 "You should insert a valid json format"
    //             );
    //         }
    //         else
    //         {
    //             $mobileUser = MobileUser::where('email', $tab->email)->get()->first();

    //             if (!empty($mobileUser))
    //             {
    //                 if ($mobileUser->type_auth_api == "gmail")
    //                 {
    //                     if(strlen($mobileUser->token_device) > 2)
    //                     {
    //                         if (!$this->SearchTokenDevice($mobileUser->token_device,$token_device))
    //                         {
    //                             $tab = (array)json_decode($mobileUser->token_device); //we should cast the object stdClass to array
    //                             $tab[] = $token_device; //add new element in the table $tab
    //                             $mobileUser->token_device = json_encode($tab) ;
    //                         }
    //                     }
    //                     else
    //                     {
    //                         $mobileUser->token_device = json_encode([$token_device]) ;
    //                     }

    //                     $mobileUser->save();
    //                     return $this->getUser($mobileUser->id);
    //                     //return $mobileUser;
    //                 }
    //                 else
    //                 {
    //                     return response()->json
    //                     (
    //                         "This email is already exist"
    //                     );
    //                 }
    //             }
    //             else
    //             {
    //                 $mobileUser = new MobileUser();
    //                 $mobileUser->nom = $tab->familyName;
    //                 $mobileUser->prenom = $tab->givenName;
    //                 $mobileUser->email = $tab->email;
    //                 $mobileUser->type_auth_api = "gmail";
    //                 $mobileUser->rappel_assurance = 2;
    //                 $mobileUser->rappel_vignette = 2;
    //                 $mobileUser->rappel_visite_technique = 2;
    //                 $mobileUser->created_at = Carbon::now();
    //                 $tab = array();
    //                 $tab[] = $token_device;
    //                 $mobileUser->token_device = json_encode($tab);
    //                 $mobileUser->new = "true";
    //                 $mobileUser->save();
    //                 return $this->getUser($mobileUser->id);
    //                 //return $this->$mobileUser;
    //             }
    //         }
    //     }
    //     catch (Exception $e)
    //     {
    //         return response()->json
    //         ("Exception error");
    //     }
    // }

    // new version 1
    // private function AuthGmail($object , $token_device)
    // {
    //     try
    //     {
    //         $verifUserByEmail = MobileUser::where('email', $object['email'])
    //                                 ->where('type_auth_api','!=','gmail')
    //                                 ->first();
    //         if(isset($verifUserByEmail))
    //         {
    //             return response()->json
    //             (
    //                 "This email is already exist"
    //             );
    //         }
    //         $mobileUser = MobileUser::where('email', $object['email'])->where('type_auth_api','gmail')->get()->first();
    //         if(isset($mobileUser))
    //         {
    //             if(strlen($mobileUser->token_device) > 2)
    //             {
    //                 if (!$this->SearchTokenDevice($mobileUser->token_device,$token_device))
    //                 {
    //                     $tab = (array)json_decode($mobileUser->token_device); //we should cast the object stdClass to array
    //                     $tab[] = $token_device; //add new element in the table $tab
    //                     $mobileUser->token_device = json_encode($tab) ;        
    //                 }
    //                 else
    //                 {
    //                     $mobileUser->token_device = json_encode([$token_device]) ;
    //                 }

    //                 $mobileUser->save();
    //                 return $this->getUser($mobileUser->id);
    //             }
    //         }
    //         else
    //         {
    //             //insert new user in database
    //             $mobileUser = new MobileUser();
    //             $mobileUser->nom = $object['familyName'];
    //             $mobileUser->prenom = $object['givenName'];
    //             $mobileUser->email = $object['email'];
    //             $mobileUser->type_auth_api = "gmail";
    //             $mobileUser->rappel_assurance = 2;
    //             $mobileUser->rappel_vignette = 2;
    //             $mobileUser->rappel_visite_technique = 2;
    //             $mobileUser->created_at = Carbon::now();
    //             $tab = array();
    //             $tab[] = $token_device;
    //             $mobileUser->token_device = json_encode($tab);
    //             $mobileUser->new = "true";
    //             $mobileUser->save();
    //             return $this->getUser($mobileUser->id);
    //         }
    //     }
    //    catch(Exception $e)
    //     {
    //          return response()->json("Exception error");
    //     }
    // }

    //new version last one
    private function AuthGmail($object)
    {
        $verifUserByEmail = MobileUser::where('email', $object->email)
                                    ->where('type_auth_api','!=','gmail')
                                    ->first();
        if(isset($verifUserByEmail))
        {
            return response()->json
            (
                "This email is already exist"
            );
        }
        $mobileUser = MobileUser::where('email', $object->email)->where('type_auth_api','gmail')->get()->first();
        if(isset($mobileUser))
        {
            if(strlen($mobileUser->token_device) > 2)
            {
                if (!$this->SearchTokenDevice($mobileUser->token_device,$object->token_device))
                    {
                        $tab = (array)json_decode($mobileUser->token_device); //we should cast the object stdClass to array
                        $tab[] = $object->token_device; //add new element in the table $tab
                        $mobileUser->token_device = json_encode($tab) ;        
                    }
                    else
                    {
                        $mobileUser->token_device = json_encode([$object->token_device]) ;
                    }

                    $mobileUser->save();
                    return $this->getUser($mobileUser->id);
                }
            }
            else
            {
                //insert new user in database
                $mobileUser = new MobileUser();
                $mobileUser->nom = $object->family_name;
                $mobileUser->prenom = $object->given_name;
                $mobileUser->email = $object->email;
                $mobileUser->type_auth_api = "gmail";
                $mobileUser->rappel_assurance = 2;
                $mobileUser->rappel_vignette = 2;
                $mobileUser->rappel_visite_technique = 2;
                $mobileUser->created_at = Carbon::now();
                $tab = array();
                $tab[] = $object->token_device;
                $mobileUser->token_device = json_encode($tab);
                $mobileUser->new = "true";
                $mobileUser->save();
                return $this->getUser($mobileUser->id);
            }
    }

    private function AuthFacebook($object, $token_device)
    {
        try
        {
            $json = (stripslashes($object));
            $tab = json_decode($json);
            $user = MobileUser::where('email', $tab->email)->first();
            if((isset($user)) AND ($user->type_auth_api != "facebook"))
                return response()->json("This user is already joined with other type_auth_api");
            elseif ((isset($user)) AND ($user->type_auth_api == "facebook"))
            {
                if(strlen($user->token_device) > 2)
                {
                    if (!$this->SearchTokenDevice($user->token_device,$token_device))
                    {
                        $tab = (array)json_decode($user->token_device); //we should cast the object stdClass to array
                        $tab[] = $token_device; //add new element in the table $tab
                        $user->token_device = json_encode($tab) ;
                    }
                }
                else
                {
                    $user->token_device = json_encode([$token_device]) ;
                }
                $user->save();
                return $this->getUser($user->id);
            }

            // creation of new account
            $newUser = new MobileUser();
            $newUser->email = $tab->email;
            $newUser->new = "true";
            $newUser->type_auth_api = "facebook";
            $newUser->rappel_assurance = 2;
            $newUser->rappel_vignette = 2;
            $newUser->rappel_visite_technique = 2;
            $tab = (array)json_decode($newUser->token_device); //we should cast the object stdClass to array
            $tab[] = $token_device; //add new element in the table $tab
            $newUser->token_device = json_encode($tab) ;
            $newUser->save();
            return $this->getUser($newUser->id);
        }
        catch(Exception $e)
        {
            return response()->json
            ("Exception error");
        }
    }

    private function AuthAppel($object,$token_device)
    {
        try
        {
            $json = (stripslashes($object));
            $tab = json_decode($json);
            if((isset($tab->email)) AND (isset($tab->user))) //the first Auth with appel we can see the email
            {
                $verifMobile = MobileUser::where('email',$tab->email)
                    ->where('appel_id', $tab->user)
                    ->first();
                if(!isset($verifMobile))
                {
                    $mobileUser = new MobileUser();
                    $mobileUser->email = $tab->email;
                    $mobileUser->type_auth_api = "appel";
                    $mobileUser->appel_id = $tab->user;
                    $mobileUser->new = "true";
                    //insert the token_device
                    $tab = array();
                    $tab[] = $token_device;
                    $mobileUser->token_device = json_encode($tab);
                    $mobileUser->save();
                    return $this->getUser($mobileUser->id);
                }
            }
            if((isset($tab->user)) AND (!isset($tab->email)))
            {
                $verifMobile = MobileUser::where('appel_id', $tab->user)->first();
                if(isset($verifMobile))
                {
                    if(strlen($verifMobile->token_device) > 2)
                    {
                        if (!$this->SearchTokenDevice($verifMobile->token_device,$token_device))
                        {
                            $tab = (array)json_decode($verifMobile->token_device); //we should cast the object stdClass to array
                            $tab[] = $token_device; //add new element in the table $tab
                            $verifMobile->token_device = json_encode($tab) ;
                        }
                    }
                    else
                    {
                        $verifMobile->token_device = json_encode([$token_device]) ;
                    }
                    $verifMobile->save();
                    return $this->getUser($verifMobile->id);
                }
                $mobileUser = new MobileUser();
                $mobileUser->type_auth_api = "appel";
                $mobileUser->appel_id = $tab->user;
                $mobileUser->new = "true";
                //insert the token_device
                $tab = array();
                $tab[] = $token_device;
                $mobileUser->token_device = json_encode($tab);
                $mobileUser->save();
                return $this->getUser($mobileUser->id);
            }
            return response()->json("This email already exist");

            //the user is already signed with
        }
        catch(Exception $e)
        {
            return response()->json("exception error");
        }

    }

    //************ Function Logout ************
    public function Logout(Request $request)
    {
        $date = date('Y-m-d H:i:s');
        $verifEmail = MobileUser::find($request->id);

        if (isset($verifEmail))
        {
            if(strlen($verifEmail->token_device)  > 2)
            {
                if ($this->SearchTokenDevice($verifEmail->token_device,$request->token_device))
                {
                    $newArray = [] ;
                    $tab = json_decode($verifEmail->token_device);
                    foreach ($tab as $key => $value)
                    {
                        if ($value != $request->token_device)
                            $newArray[] = $value ;
                    }
                    $verifEmail->token_device = isset($newArray[0]) ? json_encode($newArray) : json_encode([]) ;
                    $verifEmail->updated_at = $date;
                    $verifEmail->save();
                }
            }
            return response()->json($verifEmail);
        }
        return response()->json($verifEmail);
    }

    //************ Function InfoClient ************
    /*public function InfoClient ($idUser)
    {
        // $User = MobileUser::where('id',$idUser)->first() ;
        $User = DB::table('mobile_users')
            ->join('vehicules', 'mobile_users.id', '=', 'vehicules.id_user')
            ->select('nom','prenom','telephone','date_de_naissance','email','adresse','region','genre','token_api','type_auth_api',
            'vin','matricule','date_circulation','modele','type_assurance','puissance_fiscale','date_assurance','type_matricule','utilisation','kilometrage')
            ->where('mobile_users.id',$idUser)->first() ;

        // $User->image = str_replace("\\", "/", $User->image);
        // $User->image = asset('storage/'.$User->image);


        return response()->json($User);
    }*/
    public function InfoClient ($idUser)
    {
        try
        {
            $user = MobileUser::find($idUser);
            $vehicule = Vehicule::where('id_user',$idUser)->first();
            if(!isset($vehicule))
                $vehicule = [];
            else
                $vehicule =
                    [
                        'id' => $vehicule->id,
                        'vin' => $vehicule->vin,
                        'matricule' => $vehicule->matricule,
                        'date_circulation' => $vehicule->date_circulation,
                        'modele' => $vehicule->modele,
                        'type_assurance' => $vehicule->type_assurance,
                        'puissance' => $vehicule->puissance_fiscale,
                        'date_assurance' => $vehicule->date_assurance,
                        'type_matricule' => $vehicule->type_matricule,
                        'utilisation' => $vehicule->utilisation,
                        'date_vignette' => $vehicule->date_vignette,
                        'kilometrage' => $vehicule->kilometrage,
                        'created_at' => $vehicule->created_at,
                        'updated_at' => $vehicule->updated_at
                    ];
            $the_user =
                [
                    'id' => $user->id,
                    'nom' => $user->nom,
                    'prenom' => $user->prenom,
                    'telephone' => $user->telephone,
                    'adresse' => $user->adresse,
                    'created_at' => $user->created_at,
                    'updated_at' => $user->updated_at,
                    'date_de_naissance' => $user->date_de_naissance,
                    'email' => $user->email,
                    'new' => $user->new,
                    'vehicule' => $vehicule
                ];
            return response()->json($the_user);
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    //************ Function UpdateInfoClient ************
    public function UpdateInfoClient (Request $request) {

        $date = date('Y-m-d H:i:s');
        $pass = Hash::make($request->mot_de_passe);

        $verifUser = DB::table('mobile_users')->where('id', $request->id)->update([
            'nom' => $request->nom,
            'prenom' => $request->prenom,
            'date_de_naissance' => $request->date_de_naissance,
            'telephone' => $request->telephone,
            'region' => $request->region,
            'genre' => $request->genre,
            'email' => $request->email ,
            'adresse' => $request->adresse ,
            'mot_de_passe' => $pass,
            'updated_at' => $date
            ]);

        $VerifVehicule = DB::table('vehicules')->where('id_user', $request->id)->update([
            'vin' => $request->vin,
            'matricule' => $request->matricule,
            'type_matricule' => $request->type_matricule,
            'utilisation' => $request->utilisation,
            'date_circulation' => $request->date_circulation,
            'modele' => $request->modele,
            'type_assurance' => $request->type_assurance,
            'date_assurance' => $request->date_assurance,
            'puissance_fiscale' => $request->puissance_fiscale,
            'kilometrage' => $request->kilometrage,
            'created_at' => $date
        ]);

        return response()->json(isset($VerifVehicule) && isset($verifUser));
    }
    //************ Function WizadInfo ************
    public function WizadInfo () {

        $puissances = Puissance::where('statu',1)->get() ;
        $assurances = Assurance::where('statu',1)->get() ;

        $WizadInfo = [
            'Puissances' => $puissances,
            'Assurances' => $assurances,
        ];

        return response()->json($WizadInfo);
    }
    //************ Function Wizard ************
    public function Wizard (request $request)
    {
        $date = date('Y-m-d H:i:s');
        $date_de_naissance = Carbon::createFromDate( $request->date_de_naissance);
        $date_circulation = Carbon::createFromDate($request->date_circulation);
        $date_assurance = Carbon::createFromDate($request->date_assurance);

        $pass = Hash::make($request->mot_de_passe);
        $verifEmail = MobileUser::where('email',$request->email)->first();

        if (isset($verifEmail))
        {
            $verifEmail->nom = $request->nom;
            $verifEmail->prenom = $request->prenom;
            $verifEmail->date_de_naissance = $date_de_naissance;
            $verifEmail->telephone = $request->telephone;
            $verifEmail->genre = $request->genre;
            $verifEmail->adresse = $request->adresse;
            $verifEmail->region = $request->region;
            $verifEmail->updated_at = $date;

            if ($verifEmail->type_auth_api != "email")
            {
                $verifEmail->email =  $request->email;
                $verifEmail->mot_de_passe = $pass;
            }


            $vehicule = new Vehicule();
            $vehicule->vin = $request->vin;
            $vehicule->matricule = $request->matricule;
            $vehicule->type_matricule = $request->type_matricule; //(RS, TU, AT, CD)
            $vehicule->utilisation = $request->utilisation;
            $vehicule->date_circulation = $date_circulation;
            $vehicule->modele = $request->modele;
            $vehicule->type_assurance = $request->type_assurance;
            $vehicule->date_assurance = $date_assurance;
            $vehicule->vc = $request->vc;
            $vehicule->vo = $request->vo;
            $vehicule->puissance_fiscale = $request->puissance_fiscale;
            $vehicule->kilometrage = $request->kilometrage;
            $month = date("m",strtotime($date));
            $year = date("y",strtotime($date));
            if (intval((substr($request->matricule, -1)))%2 == 0)
            {
                if(($month >3) AND ($month <= 12))
                {
                    $year++;
                }
                $d = new \DateTime("20$year-03-05");
                $vehicule->date_vignette = $d->format('Y-m-d');
            }
            else
            {
                if(($month >4) AND ($month <= 12))
                {
                    $year++;
                }
                $d = new \DateTime("20$year-04-05");
                $vehicule->date_vignette = $d->format('Y-m-d');
            }

            $vehicule->id_user = $verifEmail->id;
            $vehicule->created_at = $date;
            $verifEmail->new = "false";
            $verifInscription = $verifEmail->save() ;
            $verifVehicule = $vehicule->save();

            return response()->json(isset($verifInscription) && isset($verifVehicule));
        }
        return response()->json(false);
    }

    //************ Function TestTokenDevice ************
    public function TestTokenDevice (Request $request)
    {
        if(isset($request->token))
        {
                $verifToken = MobileUser::where('token_device', '=', $request->token)->first();
                return response()->json(isset($verifToken));
            }
            return response()->json(false);
    }
    //************ Function TestVin ************
    public function TestVin (Request $request) {
        // return response()->json('test');

        $res = [
            'error' => '',
            'message' => '',
            'resp' => ''
        ] ;
        try 
        {
            // $response = Http::get('https://afsac-bo.switch.tn/api/aboutUs/fr');
            $response = Http::get($this->UrlGetVehicule.$request->vin);
            // return response()->json($response->status());
            if ($response->status() == 200)
            {
                if(strlen($response->body()) > 1)
                {
                    $response = json_decode($response);
                    $verifVin = Vehicule::where('vin', '=', $request->vin)->first();
                    if (isset($verifVin))
                    {
                        $verifUser = MobileUser::where('id', '=', $verifVin->id_user)
                        //    where('token_device', '=',  $request->token_device)
                        ->first();
                        if (isset($verifUser))
                        {
                            $verifVin->vc = 1;
                            $verifVin->vo = 0;
                            $verifVin->save();
                            $res['error'] = true;
                            $res['message'] = "Ce VIN est déjà associé à un autre compte utilisateur, merci de vérifier l'information introduite";
                            $res['resp'] = [];
                            return response()->json($res);
                        }
                    }
                    $verifVin->vc = 1;
                    $verifVin->vo = 0;
                    $verifVin->save();
                    $res['error'] = false ;
                    $res['message'] = "";
                    $res['resp'] = $response;
                    return response()->json($res);
                    // return response()->json($response);
                }
                else
                {
                    $res['error'] = true ;
                    $res['message'] = "VIN n'existe pas en suzuki";
                    $res['resp'] = [];
                    return response()->json($res);
                    // return response()->json("VIN n'existe pas en suzuki");
                }
            }
            else
            {
                $res['error'] = true ;
                $res['message'] = "Erreur de connexion avec le serveur";
                $res['resp'] = [];
                return response()->json($res);
                // return response()->json("Erreur api suzuki");
            }
        }
        catch (Exception $e)
        {
            return response()->json("Réessayer");
        }
    }

    public function TestVin1(Request $request)
    {
        $res = [
            'error' => '',
            'message' => '',
            'resp' => '',
            'vc' => -1,
            'vo' => -1
        ] ;
        try
        {
            if((substr( $request->vin, 0, 2 ) === "JS") or (substr( $request->vin, 0, 3 ) === "LMC")
                or (substr( $request->vin, 0, 3 ) === "MA3") or (substr( $request->vin, 0, 3 ) === "MBH")
                or (substr( $request->vin, 0, 3 ) === "MLC") or (substr( $request->vin, 0, 3 ) === "MMS") 
                or (substr( $request->vin, 0, 3 ) === "MS3")  or (substr( $request->vin, 0, 3 ) === "TSM") 
                or (substr( $request->vin, 0, 3 ) === "VSE") or (substr( $request->vin, 0, 3 ) === "VTT") 
                or (substr( $request->vin, 0, 3 ) === "8AK") 
            )
            {
                $vehicule = Vehicule::where('vin', $request->vin)->first();
                if((isset($vehicule)))
                {
                    $user = MobileUser::where('id', '=', $vehicule->id_user);
                    if (isset($user))
                    {
                        $response = Http::get($this->UrlGetVehicule . $request->vin);
                        //return response()->json($response->status());

                        if ($response->status() == 200)
                        {
                            if (strlen($response->body()) > 2)
                            {
                                $vehicule->vc = 1;
                                $vehicule->vo = 0;
                                $vehicule->save();
                                $res['error'] = true;
                                $res['message'] = "Ce VIN est déjà associé à un autre compte utilisateur, merci de vérifier l'information introduite";
                                $res['resp'] = [];
                                return response()->json($res);
                            }
                            $vehicule->vc = 0;
                            $vehicule->vo = 1;
                            $vehicule->save();
                            $res['error'] = true;
                            $res['message'] = "Ce VIN est VO";
                            $res['resp'] = [];
                            return response()->json($res);
                        }
                        else
                        {
                            $res['error'] = true;
                            $res['message'] = "Erreur de connexion avec le serveur";
                            $res['resp'] = [];
                            return response()->json($res);
                            // return response()->json("Erreur api suzuki");
                        }
                    }
                }
                else
                {
                    $response = Http::get($this->UrlGetVehicule.$request->vin);
                    // return response()->json($response->status());
                    if ($response->status() == 200)
                    {
                        if (strlen($response->body()) > 2)
                        {
                            $response = json_decode($response);
                            $res['error'] = false ;
                            $res['message'] = "";
                            $res['resp'] = $response;
                            $res['vc'] = 1;
                            $res['vo'] = 0;
                            return response()->json($res);
                        }
                        else
                        {
                            $response = json_decode($response);
                            $res['error'] = false ;
                            $res['message'] = "";
                            $res['resp'] = $response;
                            $res['vc'] = 0;
                            $res['vo'] = 1;
                            return response()->json($res);
                        }
                    }
                }
                $res['error'] = false;
                $res['message'] = "Ce vin n'existe pas dans la base de donnée";
                $res['resp'] = [];
                return response()->json($res);
            }
            else
            {
                $res['error'] = true;
                $res['message'] = "Format invalide";
                $res['resp'] = [];
                return response()->json($res);
            }
        }
        catch(Exception $e)
        {
            return response()->json("Réessayer");
        }
    }

    //************ Function AutoSendNotification ************
    public function AutoSendNotification ()
    {
        $Anniversaire = [] ;
        $Assurance = [] ;
        $VignettesExpires = [] ;
        $Other = [] ;

        $typeNotif = ["Anniversaire","Assurances","Vignettes","klm"];

        $nowDay = Carbon::now()->format('Y-m-d');
        $now = Carbon::now();
        $AllUsers = MobileUser::all();

        foreach ($AllUsers as $key => $value) {

            $vehicule = Vehicule::where('id_user',$value->id)->first() ;
            $UserDay = Carbon::createFromDate($value->date_de_naissance)->format('d');
            $UserMonth = Carbon::createFromDate($value->date_de_naissance)->format('m');

            // Push notification anniversaire
            if ((isset($value->date_de_naissance)) && ($now->format('d') == $UserDay) && ($now->format('m') == $UserMonth))
            {
                $text = "Anniversaire" ;
                $this->PushNotif($value->token_device,$text,$value->email) ;

                $notif = new Notification() ;
                $notif->titre = $text;
                $notif->valeur = $value->date_de_naissance;
                $notif->klm = null;
                $notif->type = $typeNotif[0];
                $notif->token_device = $value->token_device;
                $notif->id_user = $value->id;
                $notif->date = $nowDay;
                $notif->save();

                $Anniversaire[] = $value;
            }

            // Push notification assurance
            if  (isset($vehicule->date_assurance)) {
                if($this->DateExpire($vehicule->date_assurance,$now))
                {

                    $text = "Rappel sur le renouvellement de l’assurance" ;
                    $this->PushNotif($value->token_device,$text,$value->email) ;

                    $notif = new Notification() ;
                    $notif->titre = $text;
                    $notif->valeur = $vehicule->date_assurance;
                    $notif->klm = null;
                    $notif->type = $typeNotif[1];
                    $notif->token_device = $value->token_device;
                    $notif->id_user = $value->id;
                    $notif->date = $nowDay;
                    $notif->save();

                    $Assurance[] = $vehicule ;
                }
            }

            // Push notification Vignettes
            if  (isset($vehicule)) {
                $dateVignettes = $this->VignettesExpire($vehicule);
                if($dateVignettes['response']) {

                    $text = "Rappel sur le renouvellement Vignettes" ;
                    $this->PushNotif($value->token_device,$text,$value->email) ;

                    $notif = new Notification() ;
                    $notif->titre = $text;
                    $notif->valeur = $dateVignettes['date'];
                    $notif->klm = null;
                    $notif->type = $typeNotif[2];
                    $notif->token_device = $value->token_device;
                    $notif->id_user = $value->id;
                    $notif->date = $nowDay;
                    $notif->save();
                    $VignettesExpires[]  = $vehicule ;

                }
            }

            // Push notification entretien regulier
            if  (isset($vehicule->kilometrage) && $vehicule->kilometrage != 0) {

                $entretiens = Entretien::all() ;
                foreach ($entretiens as $key => $entretien) {

                    $varKlm = 2000 ;

                    if(($vehicule->kilometrage+$varKlm)%$entretien->kilometrage <= $varKlm) {

                        if (!$this->IsPushed($value->id,"klm",$entretien->kilometrage,$vehicule->kilometrage)) {

                            $this->PushNotif($value->token_device,$entretien->titre,$value->email) ;

                            $notif = new Notification();
                            $notif->titre = $entretien->titre;
                            $notif->valeur = $entretien->kilometrage;
                            $notif->type = $typeNotif[3];
                            $notif->klm = $vehicule->kilometrage;
                            $notif->token_device = $value->token_device;
                            $notif->id_user = $value->id;
                            $notif->date = $nowDay;
                            $notif->save();

                            $Other[$entretien->kilometrage]  = $entretien->titre ;
                        }

                    }

                }
            }

        }




        // $response = Http::withHeaders([
        //         'host' => 'exp.host',
        //         'accept' => 'application/json',
        //         'accept-encoding' => 'gzip, deflate',
        //         'content-type' => 'application/json',
        //     ])->post('https://exp.host/--/api/v2/push/send', [
        //         'to' => 'ExponentPushToken[-vcA5kCou8-Gs7AwPjcjbW]',
        //  'to' => ['ExponentPushToken[-vcA5kCou8-Gs7AwPjcjbW]','ExponentPushToken[mc3QGiEXw68_ts41TVrGvj]'],
        //         'title' => 'hello',
        //         'body' => 'world',
        // ]);

        $obj = [
            'Anniversaire ' => $Anniversaire ,
            'Assurance ' => $Assurance,
            'Vignettes ' => $VignettesExpires,
            'Other ' => $Other,
        ];

        return response()->json($obj);
    }

    //************ Function ManuelSendNotification ************
    public function ManuelSendNotification () 
    {
        $array = [] ;

        $now = Carbon::now();
        $AllUsers = MobileUser::all();
        $Notifications = ManuelNotification::all();

        foreach ($Notifications as $key => $notif) 
        {
            if($notif->now != "off")
            {
                foreach ($AllUsers as $key => $user) 
                {
                    $vehicule = Vehicule::where('id_user',$user->id)->first() ;

                    if (isset($notif->age) && (isset($notif->region)) && ($now->diff($vehicule->date_circulation)->y == $notif->age) && ($user->region == $notif->region)) 
                    {
                        $this->PushNotif($user->token_device,$notif->titre,$notif->valeur) ;
                    }
                    else if (isset($notif->age) && (!isset($notif->region)) && ($now->diff($vehicule->date_circulation)->y == $notif->age))
                    {
                        $this->PushNotif($user->token_device,$notif->titre,$notif->valeur) ;
                    }
                    else if (!isset($notif->age) && (isset($notif->region)) && ($user->region == $notif->region))
                    {
                        $this->PushNotif($user->token_device,$notif->titre,$notif->valeur) ;
                    }

                }
            }
        }
        return response()->json($array) ;
    }
    //************ Function Services ************
    public function Services () 
    {
        $services = Service::all() ;
        foreach ($services as $key => $value)
        {
            if(!isset($value->titre))
                $value->titre = "";
            if(!isset($value->adresse))
                $value->adresse = "";
            if(!isset($value->date_ouverture))
                $value->date_ouverture = ""; 
            if(!isset($value->date_fermeture))
                $value->date_fermeture = "";
            if(!isset($value->service_commercial))
                $value->service_commercial = "";
            if(!isset($value->sav))
                $value->sav = "";
            if(!isset($value->longitude))
                $value->longitude = "";
            if(!isset($value->latitude))
                $value->latitude = "";
            if(!isset($value->service_voiture))
                $value->service_voiture = "";
            $value->image = str_replace("\\", "/", $value->image);
            $value->image = asset('storage/'.$value->image);
        }
        return response()->json($services);
    }

    //************ Function ListNotification ************
    public function ListNotification ($idUser)
    {
        try
        {
            $HappyBirthday = NotifDetail::where("type",'Happy Birthday')->first();
            $BlueBell = NotifDetail::where("type","Blue Bell")->first();
            $RedBell = NotifDetail::where("type", "Red Bell")->first();
            $image = "";
            $Notifications = Notification::where('id_user',$idUser)->orderBy('created_at', 'DESC')->get(); ;
            $tab = [];
            $image = "";
            foreach($Notifications as $v)
            {
                    
                if($v->type == "3TXT")
                    $image = asset('storage/'.$RedBell->photo);
                elseif($v->type == "2TXT")
                {
                    if($v->titre == "Happy birthday")
                        $image = asset('storage/'.$HappyBirthday->photo);
                    elseif($v->titre == "Mettre à jour kilométrage")
                        $image = asset('storage/'.$RedBell->photo);
                    else
                        $image = asset('storage/'.$BlueBell->photo); 
                }
                //To fix the disply of the word "Notification" and make it empty
                if($v->titre == "Notification")
                {
                    $v->titre = $v->valeur ;
                    $v->valeur = "";
                }
                $tab[] = 
                [
                    "id" => $v->id,
                    "title" => $v->titre,
                    "smallTitle" => $v->valeur,
                    "type" => $v->type,
                    "img" => $image,
                    "date" => $v->date,
                    "color" => $v->color
                ];
            }
            return response()->json($tab);
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    //************ Function Publicite ************
    public function Publicite () 
    {
        $date = Carbon::now();

        $Publicites = Publicite::where('statu',1)
        //->where('date_debut','>=',$date)
        //->where('date_fin','<=',$date)
        ->orderBy('id', 'desc')
        ->get() ;
        

        foreach ($Publicites as $key => $value) 
        {
            $value->image = str_replace("\\", "/", $value->image);
            $value->image = asset('storage/'.$value->image);
        }
        return response()->json($Publicites);
    }


    // ************** Local Functions **************

    public function SearchTokenDevice (String $tab,String $token)
    {
        $tab = json_decode($tab);
        foreach ($tab as $key => $value)
        {
            if($value == $token)
            {
                return true;
            }
        }
        return false ;
    }

    public function PushNotif (String $token,String $title,String $body) 
    {
        $token = json_decode($token);
        $response = Http::withHeaders([
            'host' => 'exp.host',
            'accept' => 'application/json',
            'accept-encoding' => 'gzip, deflate',
            'content-type' => 'application/json',
        ])->post($this->UrlPushNotif, [
            'to' => $token,
            'title' => $title,
            'body' => $body,
        ]);

        return $response ;
    }

    public function DateExpire($date,$now) 
    {
        $nowDay = Carbon::now()->format('Y-m-d');
        return (($date>=$nowDay) && ( ($now->diffInDays($date) == 0) || ($now->diffInDays($date) == 1) || ($now->diffInDays($date)== 3) ||($now->diffInDays($date)== 7) || ($now->diffInDays($date)== 15) ||($now->diffInDays($date)== 30) ) ) ;
    }

    public function VignettesExpire($vehicule) 
    {

        $type = $vehicule->type_matricule;
        $utl = $vehicule->utilisation;

        $obj = ["response" => false,"date" => null] ;


        $now = Carbon::now();
        $m = Carbon::createFromDate()->format('m');
        $y = ($m != "12") ? Carbon::createFromDate()->format('Y') : Carbon::createFromDate()->format('Y')+1;
        // $y = $m = 12 ? Carbon::createFromDate()->format('Y'):Carbon::createFromDate()->format('Y')+1;
        $dateJanv = Carbon::createFromDate('05-01-'.$y);
        $dateFev = Carbon::createFromDate('05-02-'.$y);
        $dateMars = Carbon::createFromDate('05-03-'.$y);
        $dateAvr = Carbon::createFromDate('05-04-'.$y);

        $mat = intval(substr($vehicule->matricule,strlen($vehicule->matricule)-1,1));

        if (($type == "TU") && ($now<=$dateAvr) && ($this->DateExpire($dateAvr,$now)) && ($mat % 2 == 1) && ($utl == "PERS")) {
            $obj['response'] = true ;
            $obj['date'] = '2021-04-05' ;
            return $obj ;
        }
        else if(($type == "TU") && ($now<=$dateMars) && ($this->DateExpire($dateMars,$now)) && ($mat % 2 == 0) && ($utl == "PERS")){
            $obj['response'] = true ;
            $obj['date'] = '2021-03-05' ;
            return $obj ;
        }
        else if(($type == "TU") && ($now<=$dateFev) && ($this->DateExpire($dateFev,$now)) && ($utl == "PRO")){
            $obj['response'] = true ;
            $obj['date'] = '2021-02-05' ;
            return $obj ;
        }
        else if(($type == "RS") && ($now<=$dateFev) && ($this->DateExpire($dateFev,$now)) ){
            $obj['response'] = true ;
            $obj['date'] = '2021-02-05' ;
            return $obj ;
        }
        else if( (($type == "AT") || ($type == "CD")) && ($now<=$dateJanv) && ($this->DateExpire($dateJanv,$now)) ){
            $obj['response'] = true ;
            $obj['date'] = '2021-01-05' ;
            return $obj ;
        }

        return  $obj ;
    }

    public function IsPushed($id,$type,$valeur,$klmVehicule)
    {
        $verifNotif = Notification::where('id_user',$id)
        ->where('type',$type)
        ->where('valeur',$valeur)
        ->where('klm',$klmVehicule)
        ->first() ;

        // return $verifNotif ;
        return isset($verifNotif) ;
    }

	//added by Habib
	public function getAllCarsWS()
	{
		$url = "https://www.suzuki.tn/wp-json/mobile/get-all-cars/";
		$json = file_get_contents($url);
		$tab = json_decode($json);
		//var_dump($tab);
		$i = 0;
		foreach($tab as $v)
		{
			$i++;
			echo "$i)";
			//var_dump($v);
			echo $v->id;
			echo '<br>';
		}
		//return json_decode($json);
		//$json_data = json_decode($json, true);
	}

	public function findCarWSById($id)
	{
		$ch = file_get_contents("https://www.suzuki.tn/wp-json/mobile/get-car/$id");
		return json_encode(strstr($ch, '{'));
	}

	public function getAllUserInformationsFromCRMSystem()
    {
        $response = Http::get('http://193.95.112.185:442/ServiceCarPro.svc/getcustomer/124');
        $tab = json_decode($response);
        return $tab[0]->Vin;
    }

    private function getUser($id)
    {
        $user = MobileUser::find($id);
        $vehicule = Vehicule::where('id_user',$id)->first();
        if(!isset($vehicule))
            $vehicule = [];
        else
            $vehicule =
            [
                'id' => $vehicule->id,
                'vin' => $vehicule->vin,
                'matricule' => $vehicule->matricule,
                'date_circulation' => $vehicule->date_circulation,
                'modele' => $vehicule->modele,
                'type_assurance' => $vehicule->type_assurance,
                'puissance' => $vehicule->puissance_fiscale,
                'date_assurance' => $vehicule->date_assurance,
                'type_matricule' => $vehicule->type_matricule,
                'utilisation' => $vehicule->utilisation,
                'date_vignette' => $vehicule->date_vignette,
                'kilometrage' => $vehicule->kilometrage,
                'created_at' => $vehicule->created_at,
                'updated_at' => $vehicule->updated_at
            ];
        $the_user =
        [
            'id' => $user->id,
            'nom' => $user->nom,
            'prenom' => $user->prenom,
            'telephone' => $user->telephone,
            'adresse' => $user->adresse,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at,
            'date_de_naissance' => $user->date_de_naissance,
            'email' => $user->email,
            'new' => $user->new,
            'vehicule' => $vehicule
        ];
        return response()->json($the_user);
    }

    public function findUserById($id)
    {
        $user = MobileUser::find($id);
        if(isset($user))
            return $user;

        return response()->json([]);
    }

    public function updateUser(Request $request)
    {
        try
        {
            $user = MobileUser::find($request->id);
            if(isset($user))
            {
                $isExist = MobileUser::where('email', $request->email)->first();
                if(((isset($isExist)) AND ($user->email == $request->email)) OR (!isset($isExist)))
                {
                    $user->nom = $request->nom;
                    $user->prenom = $request->prenom;
                    $user->date_de_naissance = $request->date_de_naissance;
                    $user->email = $request->email;
                    $user->genre = $request->genre;
                    $user->telephone = $request->telephone;
                    $user->adresse = $request->adresse;
                    $user->updated_at = date('Y-m-d H:i:s');
                    $user->save();
                    //return response()->json("The modification of this user is carried out with success");
                    return $this->getUSer($user->id);
                }
                return response()->json("This email already exist");
            }
            return response()->json("This user does not exist");
        }
        catch(Exception $e)
        {
            return response()->json
            ("Exception error");
        }
    }

    public function CalculEntretien(Request $request)
    {
        //return $request;
        if(isset($request->kilometrage))
        {
            $res = $request->kilometrage/7500;

            $allEntretiens = Entretien::all('titre','kilometrage');
            $tab = [];
            foreach($allEntretiens as $v)
            {
                if(is_int($this->findNextEntretien($request->kilometrage)/$v->kilometrage))
                    $tab [] =
                        [
                            'titre' => $v->titre,
                            'kilometrage' => $v->kilometrage,
                            'result' => $this->findNextEntretien($request->kilometrage)/$v->kilometrage
                            //'isInteger' => is_int($this->findNextEntretien($request->kilometrage)/$v->kilometrage)
                        ];
            }
            return $tab;
        }
        return response()->json("Insert how many kilometre");
    }

    private function findNextEntretien($km)
    {
        if($km % 7500 == 0)
            return $km;
        else
        {
            $r = 0;
            while($km % 7500 != 0)
            {
                $km++;
            }
            return $km;
        }
    }

    public function nextEntretien($km)
    {
        return
        [
            'numberNextEntretien' => ceil($km/7500),
            'kilometrageNextEntretien' =>  $this->findNextEntretien($km)
        ];
    }

    public function updateUserVehicule(Request $request)
    {
        try
        {
            $user = MobileUser::find($request->id);
            if (isset($user))
            {
                $vehicule = Vehicule::where('id_user', $request->id)->first();
                $vehicule->vin = $request->vin;
                $vehicule->matricule = $request->matricule;
                $vehicule->date_circulation = $request->date_circulation;
                $vehicule->modele = $request->modele;
                $vehicule->type_assurance = $request->type_assurance;
                $vehicule->puissance_fiscale = $request->puissance_fiscale;
                $vehicule->date_assurance = $request->date_assurance;
                $vehicule->type_matricule = $request->type_matricule;
                $vehicule->utilisation = $request->utilisation;
                //$vehicule->date_vignette = $request->date_vignette;
                $vehicule->kilometrage = $request->kilometrage;
                $vehicule->updated_at = date('Y-m-d H:i:s');
                $vehicule->save();
                //return response()->json("The modification of this user is carried out with success");
                return $this->getUser($request->id);
            }
            return response()->json("This user does not found");
        }
        catch(Exception $e)
        {
            return response()->json("Exception error");
        }
    }

    public function rappel($id)
    {
        $vehicule = Vehicule::where('id_user',$id)->first();
        $iconAssurance = Rappel::where('titre','Assurance')->first();
        $iconVignette = Rappel::where('titre','Vignette')->first();
        $iconVisite_technique = Rappel::where('titre','Visite technique')->first();
        return response()->json
        (
            [
                [
                    'icon' => asset("/storage/$iconVisite_technique->icon"),
                    'date_visite_technique' => $this->getVisiteTechnique($vehicule->date_circulation)
                ],
                [
                    'icon' => asset("/storage/$iconAssurance->icon"),
                    'date_assurance' => $vehicule->date_assurance
                ],
                [
                    'icon' => asset("/storage/$iconVignette->icon"),
                    'date_vignette' => $this->getDateVignette($vehicule->date_vignette,$vehicule->id)
                ]
            ]
        );
    }

    public static function getVisiteTechnique($date_circulation)
    {
        $today = Carbon::today()->toDateString();
        $ageVehicule = ApisController::ageVehicule($date_circulation);
        $dateVisite = '';
        if($ageVehicule < 4)
        {
            $dateVisite = ApisController::sumDate($date_circulation, 4);
        }
        else
        {
            if(($ageVehicule == 4) AND (ApisController::sumDate($date_circulation,4) == $today ))
            {
                $dateVisite = $today;
            }
            else
            {
                if(($ageVehicule >= 4) AND (ApisController::sumDate($date_circulation, $ageVehicule) < $today))
                {
                    if(($ageVehicule % 2 == 0) AND (ApisController::sumDate($date_circulation, $ageVehicule) == $today))
                    {
                        $dateVisite = $today;
                    }
                    else
                    {
                        if(($ageVehicule % 2 == 0) AND (ApisController::sumDate($date_circulation, $ageVehicule) < $today))
                        {
                            $dateVisite = ApisController::sumDate($date_circulation, $ageVehicule+2);
                        }
                        else
                        {
                            if($ageVehicule % 2 != 0)
                            {
                                $dateVisite = ApisController::sumDate($date_circulation, $ageVehicule+2);
                            }
                        }
                    }
                }
            }
        }
        return $dateVisite;
    }

    public static function sumDate($date, $nb)
    {
        return date('Y-m-d', strtotime("+$nb year", strtotime($date)));
        //return date("Y-m-d", strtotime(date("Y-m-d", strtotime($date)) . " + $nb year"));
    }

    public static function ageVehicule($date_circulation)
    {
        $date = Carbon::parse($date_circulation);
        $now = Carbon::now();
        return Carbon::parse($date)->diffInYears();
    }

    public function getRegions()
    {
        return Region::all();
    }

    public static function getDateVignette($date, $id)
    {
        $vehicule = Vehicule::find($id);
        $today = Carbon::today()->toDateString();

        //while ($today>$date)
        {
            if ($today > $date)
            {
                //$vehicule->date_vignette = ApisController::sumDate($vehicule->date_vignette, 1);
                $vehicule->date_vignette = Carbon::parse($vehicule->date_vignette)->addYear()->format('Y-m-d');
                $vehicule->updated_at = date("Y-m-d H:m:s");
                $vehicule->save();
            }
        }
        return $vehicule->date_vignette;
    }

    public function adjustNotification(Request $request)
    {
        try
        {
            $user = MobileUser::find($request->id);
            if(isset($user))
            {
                if((($request->rappel_assurance >3) OR ($request->rappel_assurance <1)) OR
                    (($request->rappel_vignette >3) OR ($request->rappel_vignette <1)) OR
                    (($request->rappel_visite_technique >3) OR ($request->rappel_visite_technique <1)))
                    return response()->json("The value of each type of rappel should be between 1 and 3");
                $user->rappel_assurance = $request->rappel_assurance;
                $user->rappel_vignette = $request->rappel_vignette;
                $user->rappel_visite_technique = $request->rappel_visite_technique;
                /*
                    1 = ne plus me rappeler
                    2 = me rappelé ultérieur
                    3 = déjà régler
                */
                $user->save();
                return response()->json("The settings of notification is configured");
            }
            return response()->json("This user does not exist");
        }
        catch(Exception $e)
        {
            return response()->json("Exception error");
        }
    }

    public function getValueAdjustNotifs($id)
    {
        $user = MobileUser::select('id','rappel_assurance','rappel_vignette','rappel_visite_technique')->
            where('id', $id)->first();
        return $user;
    }

    public function getCountAgenceAssurance()
    {
        try
        {
            $count = Vehicule::where("type_assurance","AMI")->get()->count();
            return $count;
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    public function ContactAdmin(Request $request)
    {
        try
        {
            $verifContact = Contact::where("email", $request->email)->first();
            if(isset($verifContact))
            {
                return response()->json
                (
                    [
                        "status" => 0,
                        "title" => "error",
                        "message" => "Vous avez déjà contacté"
                    ]
                );
            }
            else
            {
                $contact = new Contact();
                $contact->id;
                $contact->nom  = $request->nom;
                $contact->prenom = $request->prenom;
                $contact->telephone = $request->telephone;
                $contact->email = strtolower($request->email);
                $contact->save();
                //insert image
                $base64_image = "data:image/jpeg;base64," . $request->photo_carte_grise;
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image))
                {
                    $contact = Contact::find($contact->id);
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $data = base64_decode($data);
                    $img = "public/contacts/contact" . $contact->id . ".gif";
                    Storage::disk('local')->put($img, $data);
                    $contact->photo_carte_grise = "contacts/contact" . $contact->id . ".gif";
                    $contact->save();
                }
                return response()->json
                (
                    [
                        "status" => 1,
                        "title" => "success",
                        "message" => "Votre demande a été envoyé avec succès"
                    ]
                );
            }
        }
        catch(Exception $e)
        {
            return response()->json
            (
                [
                    "status" => 0,
                    "title" => "exception",
                    "message" => $e->getMessage()
                ]
            );
        }
    }

    public function getDescriptionJuridique()
    {
        try
        {
            $description = DescriptionJuridique::all();
            return response()->json($description->first());
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    public function generatePDF()
    {
        $data = ['title' => 'Welcome to ItSolutionStuff.com'];
        $pdf = PDF::loadView('myPDF', $data);
  
        return $pdf->download('itsolutionstuff.pdf');
    }

    public function importExportView()
    {
       return view('import');
    }
   
    public function export() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
   
    public function import() 
    {
        Excel::import(new UsersImport,request()->file('file'));
           
        return redirect()->back();
    }

    //new feauture after the delivery of the product
    public function getModel()
    {
        try
        {
            $modeles = Modele::all();
            foreach($modeles as $v)
            {
                if(!isset($v->title))
                    $v->title = "";
                if(!isset($v->description))
                    $v->description = "";
                if(!isset($v->price_dt))
                    $v->price_dt = "" ;
                $v->logo = asset('/storage/'.$v->logo);
                $v->photo = asset('/storage/'.$v->photo);
            }
            return response()->json($modeles);
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }

    public function getModelById($id)
    {
        try
        {
            $modele = Modele::find($id);
            $modele->logo = asset('/storage/'.$modele->logo);
            $modele->photo = asset('/storage/'.$modele->photo);
            if(!isset($modele->title))
                $modele->title = "";
            if(!isset($modele->description))
                $modele->description = "";
            if(!isset($modele->price_dt))
                $modele->price_dt = "" ;
            return response()->json($modele);
        }
        catch(Exception $e)
        {
            return response()->json([]);
        }
    }
}