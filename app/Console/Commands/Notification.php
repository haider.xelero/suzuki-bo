<?php

namespace App\Console\Commands;
use DB;
use Illuminate\Console\Command;
use App\MobileUser ;

class Notification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:notif1';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push Notification to clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $users = MobileUser::all();
        DB::table('tests')->delete();
        // return $users;
    }
}
