$(document).ready
(
	function()
	{
		$("#btClick").click
        (
			function(e)
			{
                //e.preventDefault();
				var titre = $('input[name="titre"]').val();
                var valeur = $('input[name="valeur"]').val();
                var myData = $('#myForm').serializeArray();
                let vehicules_id = [];
                for(i=0; i<myData.length; i++)
                {
                    if(myData[i].name === "manuel_notification_belongstomany_vehicule_relationship[]")
                    {
                        vehicules_id.push(myData[i].value)
                    }
                }
                let json = 
                {
                    "titre" : myData[1].value,
                    "valeur" : myData[2].value,
                    "email" : myData[3].value,
                    "telephone" : myData[4].value,
                    "age" : myData[5].value,
                    "region" : myData[5].value,
                    "manuel_notification_belongstomany_vehicule_relationship" : vehicules_id
                }
                if(titre === "")
                {
                    $('input[name="titre"]').focus();
                    $(
						function() 
						{
							toastr.error("Veuillez saisir le titre de cette notification !!", 'Error');
						}
					);
                    e.preventDefault();
                }
                else
                {
                    if(valeur === "")  
                    {
                        $('input[name="valeur"]').focus();
                        $(
                            function() 
                            {
                                toastr.error("Veuillez saisir le valeur!!", 'Error');
                            }
                        );
                        e.preventDefault();
                    }
                    else
                    {
                        //Start using Ajax
                        var res=null;
               
                        $.ajax
                        (
                            {
                                async: false, 
                                type: 'POST',
                                url: "https://my.suzuki.tn/public/api/getCountUserWillNotfied",
                                data:json,
                                success: 
                                function(result)
                                {
                                    res = (result);
                                }
                            }
                        );
                        //End using Ajax
                        if (!confirm('Vous allez envoyer cette notification à '+res+" utilisateur, voulez-vous envoyer maintenanat ?")) 
                        {
                            toastr.error("Vous annulez l'envoi de la notification");
                            e.preventDefault();
                        }
                    }
                }
            }
        
        );
    }
);