@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' Vehicule')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i> </i> Véhicule hors base
        </h1>
            <a href="{{ route('voyager.vehicules.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @include('voyager::multilingual.language-selector')
    </div>
@stop
<script>
            (function(){
                    var appContainer = document.querySelector('.app-container'),
                        sidebar = appContainer.querySelector('.side-menu'),
                        navbar = appContainer.querySelector('nav.navbar.navbar-top'),
                        loader = document.getElementById('voyager-loader'),
                        hamburgerMenu = document.querySelector('.hamburger'),
                        sidebarTransition = sidebar.style.transition,
                        navbarTransition = navbar.style.transition,
                        containerTransition = appContainer.style.transition;

                    sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition =
                    appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition =
                    navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = 'none';

                    if (window.innerWidth > 768 && window.localStorage && window.localStorage['voyager.stickySidebar'] == 'true') {
                        appContainer.className += ' expanded no-animation';
                        loader.style.left = (sidebar.clientWidth/2)+'px';
                        hamburgerMenu.className += ' is-active no-animation';
                    }

                   navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = navbarTransition;
                   sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition = sidebarTransition;
                   appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition = containerTransition;
            })();
        </script>
        <script>
window.onload = function () {
    // Bulk delete selectors
    var $bulkDeleteBtn = $('#bulk_delete_btn');
    var $bulkDeleteModal = $('#bulk_delete_modal');
    var $bulkDeleteCount = $('#bulk_delete_count');
    var $bulkDeleteDisplayName = $('#bulk_delete_display_name');
    var $bulkDeleteInput = $('#bulk_delete_input');
    // Reposition modal to prevent z-index issues
    $bulkDeleteModal.appendTo('body');
    // Bulk delete listener
    $bulkDeleteBtn.click(function () {
        var ids = [];
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
        var count = $checkedBoxes.length;
        if (count) {
            // Reset input value
            $bulkDeleteInput.val('');
            // Deletion info
            var displayName = count > 1 ? 'Véhicules' : 'Véhicule enregistrés';
            displayName = displayName.toLowerCase();
            $bulkDeleteCount.html(count);
            $bulkDeleteDisplayName.html(displayName);
            // Gather IDs
            $.each($checkedBoxes, function () {
                var value = $(this).val();
                ids.push(value);
            })
            // Set input value
            $bulkDeleteInput.val(ids);
            // Show modal
            $bulkDeleteModal.modal('show');
        } else {
            // No row selected
            toastr.warning('Vous n&#039;avez sélectionné aucun élément à supprimer');
        }
    });
}
</script>
@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                            <form method="get" class="form-search">
                                <div id="search-input">
                                    <div class="col-2">
                                    </div>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="{{ __('voyager::generic.search') }}" name="s" value="">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Modele</th>
                                        <th>Client</th>
                                        <th>VIN</th>
                                        <th>Immatriculation</th>
                                        <th>Type immatriculation</th>
                                        <th>Date MeC</th>
                                        <th>Utilisation</th>
                                        <th>Assurance</th>
                                        <th>Date Assurance</th>
                                        <th>Puissance Fiscale</th>
                                        <th>Kilometrage</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        use App\Vehicule ;
                                        use App\MobileUser;
                                        $vehicules = Vehicule::where('vo',1)->get();
                                    ?>
                                    @foreach($vehicules as $v)
                                        <tr>
                                            <td>{{$v->modele}}</td>
                                            <?php
                                                $mobileUser = MobileUser::find($v->id_user);
                                            ?>
                                            @if(isset($mobileUser))
                                                <td>{{$mobileUser->prenom}}</td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>{{$v->vin}}</td>
                                            <td>{{$v->matricule}}</td>
                                            <td>{{$v->type_matricule}}</td>
                                            <td>{{$v->assurance}}</td>
                                            <td>{{$v->date_circulation}}</td>
                                            <td>{{$v->utilisation}}</td>
                                            <td>{{$v->date_assurance}}</td>
                                            <td>{{$v->puissance_fiscale}}</td>
                                            <td>{{$v->kilometrage}}</td>
                                           
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
    <link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@stop

@section('javascript')
    <!-- DataTables -->
   
          <script>
        $(document).ready(function () {
                            var table = $('#dataTable').DataTable({"order":[],"language":{"sEmptyTable":"Aucune donn\u00e9e disponible dans le tableau","sInfo":"Affichage de l'\u00e9l\u00e9ment _START_ \u00e0 _END_ sur _TOTAL_ \u00e9l\u00e9ments","sInfoEmpty":"Affichage de l'\u00e9l\u00e9ment 0 \u00e0 0 sur 0 \u00e9l\u00e9ment","sInfoFiltered":"(filtr\u00e9 \u00e0 partir de _MAX_ entr\u00e9es totales)","sInfoPostFix":"","sInfoThousands":" ","sLengthMenu":"Afficher _MENU_ \u00e9l\u00e9ments","sLoadingRecords":"Chargement...","sProcessing":"Traitement...","sSearch":"Rechercher :","sZeroRecords":"Aucun enregistrement correspondant trouv\u00e9","oPaginate":{"sFirst":"Premier","sLast":"Dernier","sNext":"Suivant","sPrevious":"Pr\u00e9c\u00e9dent"},"oAria":{"sSortAscending":": activer pour trier la colonne par ordre croissant","sSortDescending":": activer pour trier la colonne par ordre d\u00e9croissant"}},"columnDefs":[{"targets":"dt-not-orderable","searchable":false,"orderable":false}]});
            
                        $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked')).trigger('change');
            });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = 'https://suzuki.xelero.io/admin/vehicules/__id'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

                $('input[name="row_id"]').on('change', function () {
            var ids = [];
            $('input[name="row_id"]').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            $('.selected_ids').val(ids);
        });
    </script>
    
    <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
@stop