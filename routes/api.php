<?php

    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Route;

    /*
    |--------------------------------------------------------------------------
    | API Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register API routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | is assigned the "api" middleware group. Enjoy building your API!
    |
    */

    Route::middleware('auth:api')->get
    (
        '/user', function (Request $request) 
        {
            return $request->user();
        }
    );


    Route::post('/Inscription', 'ApisController@Inscription'); // valid
    Route::post('/Auth', 'ApisController@Auth'); // valid
    Route::post('/Logout', 'ApisController@Logout'); // valid
    Route::get('/InfoClient/{idUser}', 'ApisController@InfoClient'); // valid
    Route::post('/UpdateInfoClient', 'ApisController@UpdateInfoClient');
    Route::get('/WizadInfo', 'ApisController@WizadInfo'); // valid
    Route::post('/Wizard', 'ApisController@Wizard'); // valid
    Route::post('/TestVin', 'ApisController@TestVin'); // the old version 
    Route::get('/AutoSendNotification', 'ApisController@AutoSendNotification'); // problem , but we don't use it
    Route::get('/ManuelSendNotification', 'ApisController@ManuelSendNotification'); // we don't use it 
    Route::get('/Services', 'ApisController@Services'); // valid
    Route::get('/ListNotification/{idUser}', 'ApisController@ListNotification'); // valid
    Route::get('/Publicite', 'ApisController@Publicite'); // valid
    Route::get('/getRegions','ApisController@getRegions'); // valid

    // NEW API
    Route::get('/getAllCarsWS', 'ApisController@getAllCarsWS'); // option
    Route::get('/findCarWSById/{id}', 'ApisController@findCarWSById'); // option
    Route::get('/UserCRM','ApisController@getAllUserInformationsFromCRMSystem'); // we don't use it
    Route::get('/findUserById/{id}', 'ApisController@findUserById'); //  valid
    Route::post('/CalculEntretien', 'ApisController@CalculEntretien'); // valid
    Route::get('/nextEntretien/{km}','ApisController@nextEntretien'); // valid
    Route::post('/updateUser','ApisController@updateUser'); //updateUserVehicule
    Route::post('/updateUserVehicule','ApisController@updateUserVehicule'); // valid
    Route::get('/rappel/{id}', 'ApisController@rappel'); // valid
    Route::post('/adjustNotification','ApisController@adjustNotification');
    Route::get('/getValueAdjustNotifs/{id}','ApisController@getValueAdjustNotifs'); // valid
    Route::post('/TestVin1', 'ApisController@TestVin1');
    Route::get('/deleteOldNotification','ApisController@deleteOldNotification');
    Route::post('/ContactAdmin','ApisController@ContactAdmin');
    Route::get('/getDescriptionJuridique','ApisController@getDescriptionJuridique'); // valid

    //new feature after the delevry of the project
    Route::get('/getModel','ApisController@getModel'); // valid
    Route::get('/getModelById/{id}','ApisController@getModelById'); // valid
	
    //Api Cron
    Route::get('/birthday','CronController@birthday');
    Route::get('/Rappel_km','CronController@Rappel_km');
    Route::get('/Rappel_date_assurance','CronController@Rappel_date_assurance');
    Route::get('/Rappel_date_vingette','CronController@Rappel_date_vingette');
    Route::get('/Rappel_date_visite_technique','CronController@Rappel_date_visite_technique');
    Route::get('/deleteOldNotification/{id}','CronController@deleteOldNotification');

    //api test
    Route::get('/getCountAgenceAssurance','ApisController@getCountAgenceAssurance');
    Route::get('/getRole','CronController@getRole');
    Route::get('/Count', 'WebController@Count');

    //api for BO
    Route::post('/getCountUserWillNotfied','WebController@getCountUserWillNotfied');