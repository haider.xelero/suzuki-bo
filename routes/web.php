<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::post('/sendManuelleNotif','WebController@sendManuelleNotif');
Route::get('/admin/vo', 'WebController@VO' );

//____________________
Route::get('/export', 'ApisController@export')->name('export');
Route::get('importExportView', 'ApisController@importExportView');
Route::post('import', 'ApisController@import')->name('import');